import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
///components
import { FormularioComponent } from './formulario/formulario.component';
import { HomeComponent } from './home/home.component';
const routes: Routes = [

  { path: 'home', component: HomeComponent },
  { path: 'formulario1', component: FormularioComponent },
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: '**', pathMatch: 'full', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
