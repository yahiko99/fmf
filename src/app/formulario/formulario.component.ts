import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs';
import { NgWizardConfig, NgWizardService, StepChangedArgs, StepValidationArgs, STEP_STATE, THEME } from 'ng-wizard';
import { NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { Club } from '../models/club';
import { Genero } from '../models/genero';
import { clubes } from '../data/clubes';
import { generos } from '../data/generos';
import { Nacionalidad } from '../models/nacionalidad';
import { nacionalidades } from '../data/nacionalidades';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog} from "@angular/material/dialog";
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  formattedDate: string;
  nacimiento : number;
  year: number = new Date().getFullYear(); 
  clubes: Array<Club>;
  generos:Array<Genero>;
  nacionalidades: Array<Nacionalidad>;
  name: string;
  registerForm: FormGroup;
  model: NgbDateStruct;
  form: FormGroup;
  image:string;
  
  stepStates = {
    normal: STEP_STATE.normal,
    disabled: STEP_STATE.disabled,
    error: STEP_STATE.error,
    hidden: STEP_STATE.hidden
  };
 
  config: NgWizardConfig = {
    anchorSettings: {markDoneStep: true},
    selected: 0,
    theme: THEME.arrows,
    toolbarSettings: {
     
      toolbarExtraButtons: [       
        
      ],
    }
  };
  
  constructor(private ngWizardService: NgWizardService, public dialog: MatDialog, private fb: FormBuilder ) { 
    
  this.form = this.createForm(); 
 
  }
  
  ngOnInit(): void {
    this.clubes = clubes
    this.generos = generos
    this.nacionalidades = nacionalidades  
   console.log(this.form)
  }

  public downloadPDF(): void {
    const DATA = document.getElementById('htmlData');
    const doc = new jsPDF('p', 'pt', 'a4');
    const options = {
      background: 'white',
      scale: 3
    };
    html2canvas(DATA, options).then((canvas) => {
      const img = canvas.toDataURL('image/PNG');

      // Add image Canvas to PDF
      const bufferX = 15;
      const bufferY = 15;
      const imgProps = (doc as any).getImageProperties(img);
      const pdfWidth = doc.internal.pageSize.getWidth() - 2 * bufferX;
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth, pdfHeight, undefined, 'FAST');
      return doc;
    }).then((docResult) => {
      docResult.save(`${new Date().toISOString()}_tutorial.pdf`);
    });
  }

  onSubmit() {
    if (this.form.status == 'VALID') {
      console.log("es valido")
   this.config.toolbarSettings.showNextButton = true
   console.log(this.config.toolbarSettings.showNextButton)
    }
    else{
      console.log("NO es valido")
      alert("FILL ALL FIELDS")
    }
  }

  first(v: string) {
    let fecha =  v.split("-"); 
    this.formattedDate = v;
    this.nacimiento = parseInt(fecha[0]) 
  }

createForm(): FormGroup{
  return new FormGroup(
    {
      nombre: new FormControl('', [Validators.required]),    
      apellidoP: new FormControl('', [Validators.required]),    
      apellidoM: new FormControl('', [Validators.required]),   
      nacimient: new FormControl('', [Validators.required]),  
      club: new FormControl('', [Validators.required]),   
      genero: new FormControl('', [Validators.required]),   
      nacionalidad: new FormControl('', [Validators.required]),   
      rfc: new FormControl('', [ Validators.maxLength(13), Validators.minLength(13)])
    }      

  )
}

  get rfc(){ return this.form.get('rfc'); }

  getErrorRfc(): string {
     if (this.form.get('rfc').errors.required) {
      return 'Debes ingresar tu RFC! ';
    }
    if (this.form.get('rfc').errors.minLength){
  
      return 'La RFC se debe componer de minimo 13 caracteres!';
    }   
    
    if (this.form.get('rfc').errors.maxlength) {
      console.log("ya entro aqui el error")
      return 'La RFC se debe componer de 13 caracteres!';
    }
    
  }

  showPreviousStep(event?: Event) {
    this.ngWizardService.previous();
  }
 
  showNextStep(event?: Event) {
  
      this.ngWizardService.next();
 
  }
 
  resetWizard(event?: Event) {
    this.ngWizardService.reset();
  }
 
  setTheme(theme: THEME) {
    this.ngWizardService.theme(theme);
  }
 
  stepChanged(args: StepChangedArgs) {
    console.log(args.step);
  }
 
  isValidTypeBoolean: boolean = true;
 
  isValidFunctionReturnsBoolean(args: StepValidationArgs) {
    return true;
  }
 
  isValidFunctionReturnsObservable(args: StepValidationArgs) {
    return of(true);
  }



}
