import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuario } from '../models/usuario';
import { clubes } from '../data/clubes';
import { generos } from '../data/generos';
import { Club } from '../models/club';
@Injectable({
  providedIn: 'root'
})
export class DataServiceService {

    clubData: Array<Club> = clubes;

    generoData = generos;

  public url: string = 'http://localhost:4200/';

  constructor(private http: HttpClient,) { }


}
